CREATE DATABASE hotstartdb;
CREATE USER djangodb WITH PASSWORD '722c21ebfaa43e9a306c7ea4040c375c';
ALTER ROLE djangodb SET client_encoding TO 'utf8';
ALTER ROLE djangodb SET default_transaction_isolation TO 'read committed';
ALTER ROLE djangodb SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE hotstartdb TO djangodb;