# Установка Memcached
```sh
$ sudo apt-get install memcached
$ pip install python-memcached
$ sudo service memcached start
```

# Установка PostgresQL
```sh
$ sudo apt-get update
$ sudo apt-get install postgresql postgresql-contrib
```

# Создание БД
```sh
$ cd path/to/project
$ sudo -u postgres psql -f sql/create_db.sql
```

# Установка зависимостей
```sh
$ cd path/to/project
$ pip install -r requirements.txt
```

# Создание схемы БД
```sh
$ cd path/to/project
$ python manage.py makemigrations web
$ python manage.py migrate
```

# Запуск сервера
```sh
$ python manage.py runserver
```