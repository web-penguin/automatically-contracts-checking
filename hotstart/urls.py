"""hotstart URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from django.http import HttpResponseRedirect
from rest_framework_cache.registry import cache_registry
from rest_framework.authtoken import views
from constants import *
import os
from base64 import b64encode

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^', include('web.urls')),
    url(r'^$', lambda r: HttpResponseRedirect('registration/')),
    url(r'^accounts/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^token/', views.obtain_auth_token),
]

cache_registry.autodiscover()

if not os.path.isfile(SECRET_KEY_PATH):
    random_bytes = os.urandom(32)
    token = b64encode(random_bytes).decode('utf-8')
    fd = open(SECRET_KEY_PATH, 'w')
    fd.write(token)
    fd.close()

if not os.path.isfile(INCORRECT_KEY_COUNTER_PATH):
    fd = open(INCORRECT_KEY_COUNTER_PATH, 'w')
    fd.write("0")
    fd.close()

project_dirs = [TMP_PATH, ENVS_PATH, ALGORITHMS_PATH, DATASETS_PATH, TOMITA_PATH]

for dir_ in project_dirs:
    if not os.path.exists(dir_):
        os.makedirs(dir_)