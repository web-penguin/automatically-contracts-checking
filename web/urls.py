from django.conf.urls import url
from web import views
from rest_framework_swagger.views import get_swagger_view

urlpatterns = [
    url(r'^tomita-programs/$', views.TomitaList.as_view()),
    url(r'^tomita-programs/(?P<pk>[0-9]+)/$', views.TomitaDetails.as_view()),
    url(r'^experiments/$', views.ExperimentsList.as_view()),
    url(r'^experiments/(?P<pk>[0-9]+)/$', views.ExperimentDetails.as_view()),
    url(r'^algorithms/$', views.AlgorithmsList.as_view()),
    url(r'^algorithms/(?P<pk>[0-9]+)/$', views.AlgorithmDetails.as_view()),
    url(r'^results/$', views.ResultsList.as_view()),
    url(r'^results/(?P<pk>[0-9]+)/$', views.ResultDetails.as_view()),
    url(r'^files/$', views.MakeFile.as_view()),
    url(r'^files/(?P<pk>[0-9]+)/$', views.FileDetails.as_view()),
    url(r'^programs/$', views.AddProgramImplementation.as_view()),
    url(r'^programs/(?P<pk>[0-9]+)/$', views.ProgramDetails.as_view()),
    url(r'^datasets/$', views.DataSetList.as_view()),
    url(r'^datasets/(?P<pk>[0-9]+)/$', views.DataSetDetails.as_view()),
    url(r'^users/$', views.UserList.as_view()),
    url(r'^users/(?P<pk>[0-9]+)/$', views.UserDetail.as_view()),
    url(r'^upgrade/$', views.UpgradeUserRank.as_view()),
    url(r'^registration/$', views.RegisterUser.as_view()),
    url(r'^key/$', views.GetAdminKey().as_view()),
    url(r'^docs/$', get_swagger_view(title='API Docs'), name='api_docs'),
]