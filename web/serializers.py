from web.models import *
from rest_framework import serializers
from django.contrib.auth.models import User
from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry


class UserDetailsSerializer(CachedSerializerMixin):
    datasets = serializers.PrimaryKeyRelatedField(many=True, queryset=DataSet.objects.all())
    programs = serializers.PrimaryKeyRelatedField(many=True, queryset=ProgramImplementation.objects.all())
    algorithms = serializers.PrimaryKeyRelatedField(many=True, queryset=Algorithm.objects.all())

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'is_staff', 'datasets', 'programs', 'algorithms', 'is_staff')

cache_registry.register(UserDetailsSerializer)


class UserListSerializer(CachedSerializerMixin):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'is_staff')

cache_registry.register(UserListSerializer)


class DataSetListSerializer(CachedSerializerMixin):
    
    class Meta:
        model = DataSet
        fields = ('id', 'data_format', 'link', 'dataset_name')
        extra_kwargs = {
        'link': {'write_only': True},
        }

cache_registry.register(DataSetListSerializer)


class DataSetDetailsSerializer(CachedSerializerMixin):
    owner = serializers.ReadOnlyField(source='owner.username')
    
    class Meta:
        model = DataSet
        fields = ('id', 'data_format', 'dataset_name', 'link', 'owner')

cache_registry.register(DataSetDetailsSerializer)


class FileSerializer(CachedSerializerMixin):
    owner = serializers.ReadOnlyField(source='owner.username')
    
    class Meta:
        model = File
        fields = ('id', 'content', 'owner')

cache_registry.register(FileSerializer)


class OwnableDataSetsField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        try:
            user = self.context['request'].user
            queryset = DataSet.objects.filter(owner_id=user)
            return queryset
        except:
            return DataSet.objects.all()


class OwnableProgramField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        try:
            user = self.context['request'].user
            queryset = ProgramImplementation.objects.filter(owner_id=user)
            return queryset
        except:
            return ProgramImplementation.objects.all()


class ProgramImplementationListSerializer(CachedSerializerMixin):
    requirements = serializers.CharField(required=False, write_only=True)
    command_line_arguments = serializers.CharField(allow_blank=True, required=False, write_only=True)

    class Meta:
        model = ProgramImplementation
        fields = ('id', 'program_name', 'requirements', 'command_line_arguments', 'program_link')
        extra_kwargs = {
        'program_link': {'write_only': True},
        }

cache_registry.register(ProgramImplementationListSerializer)


class ProgramImplementationDetailsSerializer(CachedSerializerMixin):
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = ProgramImplementation
        fields = ('id', 'program_name', 'requirements', 'command_line_arguments', 'program_link', 'owner')

cache_registry.register(ProgramImplementationDetailsSerializer)


class ExperimentListSerializer(CachedSerializerMixin):
    dataset_id = OwnableDataSetsField()
    program_id = OwnableProgramField()

    class Meta:
        model = Experiment
        fields = ('id', 'dataset_id', 'program_id')

cache_registry.register(ExperimentListSerializer)


class ExperimentDetailsSerializer(CachedSerializerMixin):
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = Experiment
        fields = ('id', 'dataset_id', 'program_id', 'status', 'owner')

cache_registry.register(ExperimentDetailsSerializer)


class ResultListSerializer(CachedSerializerMixin):
    
    class Meta:
        model = Result
        fields = ('id', 'experiment_id', 'algorithm_id', 'answer')

cache_registry.register(ResultListSerializer)


class ResultDetailsSerializer(CachedSerializerMixin):
    owner = serializers.ReadOnlyField(source='owner.username')
    
    class Meta:
        model = Result
        fields = ('id', 'experiment_id', 'algorithm_id', 'start_time', 'end_time', 'answer', 'owner')

cache_registry.register(ResultDetailsSerializer)


class AlgorithmSerializer(CachedSerializerMixin):
    owner = serializers.ReadOnlyField(source='owner.username')
    dataset_id = OwnableDataSetsField()
    headings = serializers.CharField(required=False)

    class Meta:
        model = Algorithm
        fields = ('id', 'dataset_id', 'algorithm', 'headings', 'owner')

cache_registry.register(AlgorithmSerializer)


class TomitaSerializer(CachedSerializerMixin):
    owner = serializers.ReadOnlyField(source='owner.username')
    info = serializers.ReadOnlyField()

    class Meta:
        model = TomitaExecution
        fields = ('id', 'link_to_configs', 'info', 'owner')

cache_registry.register(TomitaSerializer)


class CurrentUserUpgradeSerializer(serializers.ModelSerializer):
    admin_key = serializers.CharField()

    class Meta:
        model = User
        fields = ('admin_key',)


class RegisterUserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    confirm_password = serializers.CharField(write_only=True)
    admin_key = serializers.CharField(allow_blank=True, required=False, write_only=True)

    class Meta:
        model = User
        fields = ("id", "username", "email", "password", "confirm_password", "admin_key")

    def validate(self, attrs):
        if attrs.get('password') != attrs.get('confirm_password'):
            raise serializers.ValidationError("Those passwords don't match.")
        return attrs
