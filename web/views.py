from web.models import *
from web.serializers import *
from web.permissions import IsOwnerOrAdmin, IsSelfOrAdmin
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics
from django.contrib.auth.models import User
from rest_framework import permissions
import urllib
from django.contrib.auth.models import User
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError as ValidationErrorURL
import json
import hashlib
import zipfile
import os
import subprocess
import re
import datetime
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.layout import LAParams, LTTextBox, LTTextLine
from pdfminer.converter import PDFPageAggregator
import textract
from django.shortcuts import redirect
from subprocess import CalledProcessError
import random
from docx import Document
from constants import *


def extractTextFromPDF(path, toFile):
    extractedText = ""
    fd = open(path, "rb")
    parser = PDFParser(fd)
    document = PDFDocument(parser)
    rsrcmgr = PDFResourceManager()
    laparams = LAParams()
    device = PDFPageAggregator(rsrcmgr, laparams=laparams)
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    for page in PDFPage.create_pages(document):
        interpreter.process_page(page)
        layout = device.get_result()
        for lt_obj in layout:
            if isinstance(lt_obj, LTTextBox) or isinstance(lt_obj, LTTextLine):
                extractedText += lt_obj.get_text()
    fd.close()
    if toFile:
        pathToSave = path[:-(len(".pdf"))] + ".txt"
        fd = open(pathToSave, 'w')
        fd.write(extractedText)
        fd.close()
        return pathToSave
    else:
        return extractedText


def extractTextFromWordFiles(path, type):
    extractedText = textract.process(path)
    pathToSave = path[:-len(type)] + ".txt"
    fd = open(pathToSave, 'w')
    fd.write(extractedText.decode('utf-8'))
    fd.close()
    return pathToSave


def installRequirements(text, user):
    isLink = False
    try:
        URLValidator()(text)
        isLink = True
    except ValidationErrorURL:
        pass
    if not isLink:
        pattern = re.compile("(\w+[\<\=\>]*[\d\.]*)+")
        libraries = pattern.findall(text)
        text = "\n".join(libraries)
    md5 = str(hashlib.md5((text + str(random.randrange(100))).encode("utf-8")).hexdigest())
    path = TMP_PATH + "%s.txt" % md5
    if isLink:
        urllib.request.urlretrieve(text, path)
    else:
        fd = open(path, 'w')
        fd.write(text)
        fd.close()
    if not os.path.isdir(ENVS_PATH + user):
        subprocess.call("virtualenv -p " + PYTHON_COMMAND + ENVS_PATH + user, shell=True)
    subprocess.call(ENVS_PATH + "%s/bin/pip install -r %s" % (user, path), shell=True)
    os.remove(path)
    return


def changeKey(fd, key):
    fd.seek(0)
    fd.truncate()
    newMD5 = str(hashlib.md5((str(random.randrange(10001)) + key + SALT).encode("utf-8")).hexdigest())
    fd.write(newMD5)
    fd.close()
    return


def changeCounter(fd, value):
    fd.seek(0)
    fd.truncate()
    fd.write(str(value))
    fd.close()
    return


def getHeadings(path):
    document = Document(path)
    headings = []
    for paragraph in document.paragraphs:
        text = paragraph.text.replace('\xa0', '')
        if paragraph.style.name.startswith('Heading'):
            headings.append(text)
            continue
        counter = 0
        if len(paragraph.runs) == 1 or (len(paragraph.runs) == 2 and paragraph.runs[1].text == '\xa0'):
            try:
                if text == re.match(r'\d+\.\ ?[А-Я\Ё][А-Я\Ё\ \,\-]+[\.\:]?\ ?', text).group(0):
                    headings.append(text)
                    continue
                elif len(re.findall(r'\d+\.\ ?[А-Я\Ё]', text)) > 0:
                    counter += 1
            except:
                pass
            if paragraph.runs[0].bold:
                counter += 1
            if paragraph.alignment == 1 and text != "":
                counter += 1
        if counter >= 2:
            headings.append(text)
    return headings


def makeHeadingsDict(headings, localHeadings):
    if headings == "":
        return {}
    inputHeadings = headings.split(';')
    headingsDict = {}
    for heading in inputHeadings:
        heading = heading.strip()
        headingsDict[heading] = any(heading.lower() in t.lower() for t in localHeadings)
    return headingsDict


class UserList(generics.ListAPIView):
    """
    get:
    Return a list of all the existing users with short information.
    """

    serializer_class = UserListSerializer

    def get_queryset(self, *args, **kwargs):
        if not self.request.user.is_staff:
            return User.objects.filter(pk=self.request.user.id)
        else:
            return User.objects.all()


class UserDetail(generics.RetrieveAPIView):
    """
    get:
    Return a detailed information about user
    """

    queryset = User.objects.all()
    serializer_class = UserDetailsSerializer
    permission_classes = (IsSelfOrAdmin,)


class DataSetList(generics.ListCreateAPIView):
    """
    get:
    Return a list of all the existing datasets with short information.
    post:
    Create a new instance of dataset in the system.
    """

    serializer_class = DataSetListSerializer
    
    def get_queryset(self, *args, **kwargs):
        if not self.request.user.is_staff:
            return DataSet.objects.filter(owner=self.request.user)
        else:
            return DataSet.objects.all()

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def post(self, request):
        serializer = DataSetListSerializer(data=request.data)
        if serializer.is_valid():
            link = serializer.validated_data['link']
            dataFormat = serializer.validated_data['data_format']
            try:
                URLValidator()(link)
                md5 = str(hashlib.md5((link + dataFormat + str(self.request.user)).encode("utf-8")).hexdigest())
                zipDir = DATASETS_PATH + md5
                if not os.path.isdir(zipDir):
                    pathToArchive = DATASETS_PATH + "%s.zip" % md5
                    urllib.request.urlretrieve(link, pathToArchive)
                    zip_ref = zipfile.ZipFile(pathToArchive, 'r')
                    zip_ref.extractall(zipDir)
                    zip_ref.close()
                    os.remove(pathToArchive)
                dirNameFromZip = "/" + os.listdir(zipDir)[0]
                fileNames = os.listdir(zipDir + dirNameFromZip)
                for i in fileNames:
                    if i[-(len(dataFormat) + 1):] != "." + dataFormat.lower():
                        os.remove(zipDir + dirNameFromZip + "/" + i)
            except ValidationErrorURL:
                if dataFormat != "TXT":
                    return Response("Files in database have txt format", status=status.HTTP_400_BAD_REQUEST)
                ids = re.findall(r'\d+', link)
                serializer.validated_data['link'] = ",".join(ids)
            serializer.save(owner=self.request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DataSetDetails(generics.RetrieveDestroyAPIView):
    """
    get:
    Return a detailed information about dataset.
    delete:
    Destroy certain dataset instance.
    """
    queryset = DataSet.objects.all()
    serializer_class = DataSetDetailsSerializer
    permission_classes = (IsOwnerOrAdmin,)


class AddProgramImplementation(generics.ListCreateAPIView):
    """
    get:
    Return a list of all the existing programs with short information.
    post:
    Create a new instance of program in the system.
    """

    serializer_class = ProgramImplementationListSerializer

    def get_queryset(self, *args, **kwargs):
        if not self.request.user.is_staff:
            return ProgramImplementation.objects.filter(owner=self.request.user)
        else:
            return ProgramImplementation.objects.all()

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def post(self, request):
        serializer = ProgramImplementationListSerializer(data=request.data)
        if serializer.is_valid():
            currentUsername = self.request.user
            link = serializer.validated_data['program_link']
            md5 = str(hashlib.md5((link + str(currentUsername)).encode("utf-8")).hexdigest())
            pathToFile = ALGORITHMS_PATH + "%s.py" % md5
            urllib.request.urlretrieve(link, pathToFile)
            if 'requirements' in serializer.validated_data.keys():
                installRequirements(serializer.validated_data['requirements'], str(currentUsername))
            serializer.save(owner=currentUsername)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProgramDetails(generics.RetrieveDestroyAPIView):
    """
    get:
    Return a detailed information about program.
    delete:
    Destroy certain program instance.
    """

    queryset = ProgramImplementation.objects.all()
    serializer_class = ProgramImplementationDetailsSerializer
    permission_classes = (IsOwnerOrAdmin,)


class MakeFile(generics.ListCreateAPIView):
    """
    get:
    Return a list of all the existing text files in the system.
    post:
    Create a new instance of text file in the system.
    """

    serializer_class = FileSerializer

    def get_queryset(self, *args, **kwargs):
        if not self.request.user.is_staff:
            return File.objects.filter(owner=self.request.user)
        else:
            return File.objects.all()

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class FileDetails(generics.RetrieveDestroyAPIView):
    """
    get:
    Return information about text file.
    delete:
    Destroy certain text file instance.
    """

    queryset = File.objects.all()
    serializer_class = FileSerializer
    permission_classes = (IsOwnerOrAdmin,)


class ExperimentsList(generics.ListCreateAPIView):
    """
    get:
    Return a list of all the existing experiments with short information.
    post:
    Create a new instance of experiment(program connected with dataset) in the system.
    """

    serializer_class = ExperimentListSerializer

    def get_queryset(self, *args, **kwargs):
        if not self.request.user.is_staff:
            return Experiment.objects.filter(owner=self.request.user)
        else:
            return Experiment.objects.all()

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def post(self, request):
        serializer = ExperimentListSerializer(data=request.data)
        if serializer.is_valid():
            dataset = serializer.validated_data['dataset_id']
            program = serializer.validated_data['program_id']
            currentUsername = self.request.user
            if dataset.owner != currentUsername:
                return Response("You are not an owner of this dataset", status=status.HTTP_403_FORBIDDEN)
            elif program.owner != currentUsername:
                return Response("You are not an owner of this program", status=status.HTTP_403_FORBIDDEN)
            commandLineArguments = program.command_line_arguments
            md5 = str(hashlib.md5((program.program_link + str(currentUsername)).encode("utf-8")).hexdigest())
            pathToProgram = ALGORITHMS_PATH + "%s.py" % md5
            try:
                URLValidator()(dataset.link)
            except ValidationErrorURL:
                ids = dataset.link.split(",")
                filesDict = {}
                for i in ids:
                    filesDict[i] = File.objects.get(pk=int(i))
                answerDict = {}
                startTime = datetime.datetime.now()
                for ID, file in filesDict.items():
                    path = TMP_PATH + str(hashlib.md5((ID + str(currentUsername) + str(random.randrange(100))).encode("utf-8")).hexdigest())
                    fd = open(path, 'w')
                    fd.write(file.content)
                    fd.close()
                    try:
                        resultString = subprocess.check_output(ENVS_PATH + "%s/bin/python %s %s < %s" % \
                            (str(currentUsername), pathToProgram, commandLineArguments, path), shell=True).decode('utf-8')
                    except CalledProcessError:
                        os.remove(path)
                        serializer.save(owner=currentUsername, status='INC')
                        return Response("Programm error", status=status.HTTP_422_UNPROCESSABLE_ENTITY)
                    answerDict[ID] = resultString
                    os.remove(path)
                endTime = datetime.datetime.now()
                experiment = serializer.save(owner=currentUsername, status='FIN')
                result = Result.objects.create(experiment_id=experiment, start_time=startTime, end_time=endTime, answer=answerDict, owner=currentUsername)
                return redirect('/results/%d/' % result.id)

            inputDataFormat = dataset.data_format
            hashForDirName = str(hashlib.md5((dataset.link + inputDataFormat + str(currentUsername)).encode("utf-8")).hexdigest())
            tmp = DATASETS_PATH + hashForDirName + "/"
            pathToDirWithInputFiles = tmp + os.listdir(tmp)[0]
            filePaths = []
            answerDict = {}
            startTime = datetime.datetime.now()
            if inputDataFormat == 'DOCX' or inputDataFormat == 'DOC':
                print(pathToDirWithInputFiles)
                for i in os.listdir(pathToDirWithInputFiles):
                    pathToResult = extractTextFromWordFiles(pathToDirWithInputFiles + "/" + i, inputDataFormat)
                    try:
                        resultString = subprocess.check_output(ENVS_PATH + "%s/bin/python %s %s < %s" % \
                            (str(currentUsername), pathToProgram, commandLineArguments, pathToResult), shell=True).decode('utf-8')
                    except CalledProcessError:
                        os.remove(pathToResult)
                        serializer.save(owner=currentUsername, status='INC')
                        return Response("Programm error", status=status.HTTP_422_UNPROCESSABLE_ENTITY)
                    answerDict[i] = resultString
                    os.remove(pathToResult)
            elif inputDataFormat == 'PDF':
                for i in os.listdir(pathToDirWithInputFiles):
                    pathToResult = extractTextFromPDF(pathToDirWithInputFiles + "/" + i, True)
                    try:
                        resultString = subprocess.check_output(ENVS_PATH + "%s/bin/python %s %s < %s" % \
                            (virtualenvName, pathToProgram, commandLineArguments, pathToResult), shell=True).decode('utf-8')
                    except CalledProcessError:
                        os.remove(pathToResult)
                        serializer.save(owner=currentUsername, status='INC')
                        return Response("Programm error", status=status.HTTP_422_UNPROCESSABLE_ENTITY)
                    answerDict[i] = resultString
                    os.remove(pathToResult)
            else:
                for i in os.listdir(pathToDirWithInputFiles):
                    try:
                        resultString = subprocess.check_output(ENVS_PATH + "%s/bin/python %s %s < %s/%s" % \
                            (virtualenvName, pathToProgram, commandLineArguments, pathToDirWithInputFiles, i), shell=True).decode('utf-8')
                    except CalledProcessError:
                        serializer.save(owner=currentUsername, status='INC')
                        return Response("Programm error", status=status.HTTP_422_UNPROCESSABLE_ENTITY)
                    answerDict[i] = resultString
            endTime = datetime.datetime.now()
            experiment = serializer.save(owner=currentUsername, status='FIN')
            result = Result.objects.create(experiment_id=experiment, start_time=startTime, end_time=endTime, answer=answerDict, owner=currentUsername)
            return redirect('/results/%d/' % result.id)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ExperimentDetails(generics.RetrieveDestroyAPIView):
    """
    get:
    Return full information about certain experiment.
    delete:
    Destroy certain experiment instance.
    """

    queryset = Experiment.objects.all()
    serializer_class = ExperimentDetailsSerializer
    permission_classes = (IsOwnerOrAdmin,)


class ResultsList(generics.ListAPIView):
    """
    get:
    Return a list of all the existing results with short information.
    """

    serializer_class = ResultListSerializer

    def get_queryset(self, *args, **kwargs):
        if not self.request.user.is_staff:
            return Result.objects.filter(owner=self.request.user)
        else:
            return Result.objects.all()


class ResultDetails(generics.RetrieveDestroyAPIView):
    """
    get:
    Return full information about result
    delete:
    Destroy certain result instance.
    """

    queryset = Result.objects.all()
    serializer_class = ResultDetailsSerializer
    permission_classes = (IsOwnerOrAdmin,)


class AlgorithmsList(generics.ListCreateAPIView):
    """
    get:
    Return a list of all the existing algorithms.
    post:
    Create a new instance -- application of the algorithm on dataset and return the result.
    """

    serializer_class = AlgorithmSerializer

    def get_queryset(self, *args, **kwargs):
        if not self.request.user.is_staff:
            return Algorithm.objects.filter(owner=self.request.user)
        else:
            return Algorithm.objects.all()

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def post(self, request):
        serializer = AlgorithmSerializer(data=request.data)
        if serializer.is_valid():
            dataset = serializer.validated_data['dataset_id']
            currentUsername = self.request.user
            if dataset.owner != currentUsername:
                return Response("You are not an owner of this dataset", status=status.HTTP_403_FORBIDDEN)
            inputDataFormat = dataset.data_format
            algorithm = serializer.validated_data['algorithm']
            headings = ""
            if 'headings' in serializer.validated_data.keys():
                headings = serializer.validated_data['headings']
            obj = serializer.save(owner=currentUsername)
            hashForDirName = str(hashlib.md5((dataset.link + inputDataFormat + str(currentUsername)).encode("utf-8")).hexdigest())
            tmp = DATASETS_PATH + "%s/" % hashForDirName
            pathToDirWithInputFiles = tmp + os.listdir(tmp)[0]
            answerDict = {}
            startTime = datetime.datetime.now()
            if inputDataFormat == 'DOC':
                for i in os.listdir(pathToDirWithInputFiles):
                    subprocess.call(['soffice', '--headless', '--convert-to', \
                        'docx', '--outdir', pathToDirWithInputFiles, "%s/%s" % (pathToDirWithInputFiles, i)])
            if inputDataFormat == "DOCX" or inputDataFormat == "DOC":
                for file in os.listdir(pathToDirWithInputFiles):
                    if file.endswith(".docx"):
                        localHeadings = getHeadings("%s/%s" % (pathToDirWithInputFiles, file))
                        if inputDataFormat == 'DOC':
                            os.remove("%s/%s" % (pathToDirWithInputFiles, file))
                            if algorithm == 'EXH':
                                answerDict[file[:-1]] = localHeadings
                            else:
                                answerDict[file[:-1]] = makeHeadingsDict(headings, localHeadings)
                        else:
                            if algorithm == 'EXH':
                                answerDict[file] = localHeadings
                            else:
                                answerDict[file] = makeHeadingsDict(headings, localHeadings)
            if inputDataFormat == "PDF":
                for file in os.listdir(pathToDirWithInputFiles):
                    if file.endswith(".pdf"):
                        text = extractTextFromPDF("%s/%s" % (pathToDirWithInputFiles, file), False)
                        localHeadings.extend(re.findall(r'\n\d+\.\ [А-Я\Ё][а-я\ё\ \,\-\]+[\.\:]*\n', text))
                        localHeadings.extend(re.findall(r'\n\d+\.\ [А-Я\Ё][А-Я\Ё\ \,\-\]+[\.\:]*\n', text))
                        if algorithm == 'EXH':
                                answerDict[file] = localHeadings
                        else:
                            answerDict[file] = makeHeadingsDict(headings, localHeadings)
            endTime = datetime.datetime.now()
            result = Result.objects.create(algorithm_id=obj, start_time=startTime, end_time=endTime, answer=answerDict, owner=currentUsername)
            return redirect('/results/%d/' % result.id)


class AlgorithmDetails(generics.RetrieveDestroyAPIView):
    """
    get:
    Return information about certain algorithm.
    delete:
    Destroy certain algorithm instance.
    """

    queryset = Algorithm.objects.all()
    serializer_class = AlgorithmSerializer
    permission_classes = (IsOwnerOrAdmin,)


class TomitaList(generics.ListCreateAPIView):
    """
    get:
    Return a list of all the tomita executions.
    post:
    Create a new instance of tomita execution and return the result.
    """

    serializer_class = TomitaSerializer

    def get_queryset(self, *args, **kwargs):
        if not self.request.user.is_staff:
            return TomitaExecution.objects.filter(owner=self.request.user)
        else:
            return TomitaExecution.objects.all()

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def post(self, request):
        serializer = TomitaSerializer(data=request.data)
        if serializer.is_valid():
            currentUsername = self.request.user
            linkToConfigs = serializer.validated_data['link_to_configs']
            print(linkToConfigs)
            try:
                URLValidator()(linkToConfigs)
            except:
                return Response("Not a link to tomita configs", status=status.HTTP_400_BAD_REQUEST)
            md5 = str(hashlib.md5((linkToConfigs + str(currentUsername)).encode("utf-8")).hexdigest())
            zipDir = TOMITA_PATH + md5
            if not os.path.isdir(zipDir):
                pathToArchive = TOMITA_PATH + "%s.zip" % md5
                urllib.request.urlretrieve(linkToConfigs, pathToArchive)
                zip_ref = zipfile.ZipFile(pathToArchive, 'r')
                zip_ref.extractall(zipDir)
                zip_ref.close()
                os.remove(pathToArchive)
            dirNameFromZip = "/" + os.listdir(zipDir)[0]
            fileNames = os.listdir(zipDir + dirNameFromZip)
            answerDict = {'result': ""}
            if 'config.proto' in fileNames:
                output = subprocess.check_output(TOMITA_PATH + "./%s config.proto" % TOMITA_EXECUTOR, cwd=zipDir + dirNameFromZip, shell=True)
                answerDict['output'] = output.decode('utf-8')
                newFiles = os.listdir(zipDir + dirNameFromZip)
                newFiles = [i for i in newFiles if i not in fileNames]
                for file in newFiles:
                    if not file.endswith('.bin'):
                        fd = open(zipDir + dirNameFromZip + '/' + file, 'r')
                        content = fd.read()
                        fd.close()
                        answerDict['result'] = content
                        os.remove(zipDir + dirNameFromZip + '/' + file)
                serializer.save(owner=currentUsername, info=answerDict)
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response("No config.proto file", status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TomitaDetails(generics.RetrieveDestroyAPIView):
    """
    get:
    Return information about certain tomita execution.
    delete:
    Destroy certain tomita execution instance.
    """

    queryset = TomitaExecution.objects.all()
    serializer_class = TomitaSerializer
    permission_classes = (IsOwnerOrAdmin,)


class RegisterUser(generics.CreateAPIView):
    """
    post:
    Register new user
    """

    serializer_class = RegisterUserSerializer
    permission_classes = (permissions.AllowAny,)

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            fd = open(SECRET_KEY_PATH, 'r+')
            key = fd.read()
            fd2 = open(INCORRECT_KEY_COUNTER_PATH, 'r+')
            incorrectKeyCounter = int(fd2.read())
            tmpDict = serializer.validated_data
            if tmpDict['admin_key'] == key:
                changeKey(fd, key)
                incorrectKeyCounter = 0
                changeCounter(fd2, incorrectKeyCounter)
            elif tmpDict['admin_key'] != "":
                if incorrectKeyCounter >= 4:
                    incorrectKeyCounter = 0
                    changeKey(fd, key)
                else:
                    incorrectKeyCounter += 1
                    fd.close()
                changeCounter(fd2, incorrectKeyCounter)
            User.objects.create_user(tmpDict['username'], tmpDict['email'], tmpDict['password'], is_staff=tmpDict['admin_key'] == key)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UpgradeUserRank(generics.UpdateAPIView):
    """
    put:
    Change user rank from usual to staff.
    patch:
    Change user rank from usual to staff.
    """

    serializer_class = CurrentUserUpgradeSerializer

    def put(self, request):
        serializer = CurrentUserUpgradeSerializer(data=request.data)
        user = request.user
        if serializer.is_valid():
            fd = open(SECRET_KEY_PATH, 'r+')
            key = fd.read()
            fd2 = open(INCORRECT_KEY_COUNTER_PATH, 'r+')
            incorrectKeyCounter = int(fd2.read())
            if serializer.data['admin_key'] == key:
                user.is_staff = True
                user.save()
                changeKey(fd, key)
                incorrectKeyCounter = 0
                changeCounter(fd2, incorrectKeyCounter)
                return Response("Key is OK", status=status.HTTP_200_OK)
            else:
                if incorrectKeyCounter >= 4:
                    incorrectKeyCounter = 0
                    changeKey(fd, key)
                else:
                    incorrectKeyCounter += 1
                    fd.close()
                changeCounter(fd2, incorrectKeyCounter)
                return Response("Key is not OK", status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GetAdminKey(APIView):
    """
    get:
    Return admin key to get staff group by other user.
    """

    def get(self, request):
        if request.user.is_staff:
            fd = open(SECRET_KEY_PATH, 'r')
            key = fd.read()
            fd.close()
            return Response(key, status=status.HTTP_200_OK)
        else:
            return Response("You are not admin", status=status.HTTP_403_FORBIDDEN)
