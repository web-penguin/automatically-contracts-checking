from django.db import models
from django.contrib.postgres.fields import JSONField

class File(models.Model):
    content = models.TextField()
    owner = models.ForeignKey('auth.User', related_name='file', on_delete=models.CASCADE)


class DataSet(models.Model):
    FORMAT_CHOICES = (
        ('TXT', 'Text file'),
        ('PDF', 'Portable document format'),
        ('DOC', 'Word document'),
        ('DOCX', 'Word document with XML'),
    )

    data_format = models.CharField(max_length=4, choices=FORMAT_CHOICES)
    link = models.TextField()
    dataset_name = models.CharField(max_length=50)
    owner = models.ForeignKey('auth.User', related_name='datasets', on_delete=models.CASCADE)

    def __str__(self):
        return self.dataset_name


class ProgramImplementation(models.Model):
    program_name = models.CharField(max_length=50)
    requirements = models.TextField(null=True)
    command_line_arguments = models.TextField(blank=True)
    program_link = models.TextField()
    owner = models.ForeignKey('auth.User', related_name='programs', on_delete=models.CASCADE)

    def __str__(self):
        return self.program_name

class Algorithm(models.Model):

    ALGORITHM_CHOICE = (
    ('EXH', 'Extract Headings'),
    ('CHH', 'Check Headings'),
    )

    dataset_id = models.ForeignKey(DataSet, on_delete=models.CASCADE)
    algorithm = models.CharField(max_length=3, choices=ALGORITHM_CHOICE)
    headings = models.TextField(null=True)
    owner = models.ForeignKey('auth.User', related_name='algorithms', on_delete=models.CASCADE)


class Experiment(models.Model):
    EXPERIMENT_STATUS = (
    ('RUN', 'Running'),
    ('INC', 'Incorrect'),
    ('FIN', 'Finished'),
    )

    dataset_id = models.ForeignKey(DataSet, related_name='datasetid', on_delete=models.CASCADE)
    program_id = models.ForeignKey(ProgramImplementation, related_name='programid', on_delete=models.CASCADE)
    status = models.CharField(max_length=3, choices=EXPERIMENT_STATUS)
    owner = models.ForeignKey('auth.User', related_name='experiments', on_delete=models.CASCADE)


class Result(models.Model):
    experiment_id = models.ForeignKey(Experiment, related_name='experimentid', on_delete=models.CASCADE, null=True)
    algorithm_id = models.ForeignKey(Algorithm, related_name='algorithmid', on_delete=models.CASCADE, null=True)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    answer = JSONField()
    owner = models.ForeignKey('auth.User', related_name='results', on_delete=models.CASCADE)


class TomitaExecution(models.Model):
    link_to_configs = models.TextField()
    info = JSONField()
    owner = models.ForeignKey('auth.User', related_name='tomitas', on_delete=models.CASCADE)